import React from 'react';
import {SafeAreaView, StyleSheet, View, Text, StatusBar} from 'react-native';
// import {ListProvider} from './src/screens/Tugas4/Index';
import {ListProvider} from './src/screens/MiniProject1/Index';
import {Colors} from 'react-native/Libraries/NewAppScreen';

// import Tugas1 from './src/screens/Tugas1/Tugas1';
// import Index from './src/screens/Tugas2/Index.js';
// import TodoList from './src/screens/Tugas3/TodoList.js';
// import TodoList from './src/screens/Tugas4/TodoList';
import PhoneContact from './src/screens/MiniProject1/PhoneContact';

const App: () => React$Node = () => {
  return (
    <>
      <ListProvider>
        <SafeAreaView style={styles.container}>
          <StatusBar backgroundColor="#3A86FF" barStyle="light-content" />
          <View style={styles.viewNavbar}>
            {/* <Text style={{color: 'white', fontSize: 20}}>Account</Text> */}
            <Text style={{color: 'white', fontSize: 20}}>Daftar Kontak</Text>
          </View>
          <View style={styles.viewMain}>
            {/* <Tugas1 /> */}
            {/* <Index /> */}
            <PhoneContact />
            {/* <AppNavigation /> */}
          </View>
        </SafeAreaView>
      </ListProvider>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewNavbar: {
    backgroundColor: '#3A86FF',
    height: 54,
    justifyContent: 'center',
    paddingLeft: 20,
  },
  viewMain: {
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
});

export default App;
