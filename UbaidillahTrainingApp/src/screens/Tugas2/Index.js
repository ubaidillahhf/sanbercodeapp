import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

import exampleImage from '../src/assets/images/ubaidillah.jpg';
import Icon from 'react-native-vector-icons/FontAwesome';
const exampleImageUri = Image.resolveAssetSource(exampleImage).uri;

const Index = () => {
  return (
    <View style={styles.viewComponent}>
      <View style={styles.viewPhotoName}>
        <Image
          source={{
            uri: exampleImageUri,
          }}
          style={styles.profileImg}
        />
        <Text style={styles.viewTextName}>Ubaidillah Hakim Fadly</Text>
      </View>
      <View style={styles.viewParentWithMarginBottom}>
        <View style={{flexDirection: 'row'}}>
          <Icon
            name="wpexplorer"
            style={{fontSize: 25, textAlignVertical: 'center'}}
          />
          <Text style={styles.textProfile}>Saldo</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{textAlignVertical: 'center', fontWeight: 'bold'}}>
            Rp. 120.000.000
          </Text>
        </View>
      </View>
      <View style={styles.viewParentWithBorderBottom}>
        <View style={{flexDirection: 'row'}}>
          <Icon
            name="gears"
            style={{fontSize: 25, textAlignVertical: 'center'}}
          />
          <Text style={styles.textProfile}>Pengaturan</Text>
        </View>
      </View>
      <View style={styles.viewParentWithBorderBottom}>
        <View style={{flexDirection: 'row'}}>
          <Icon
            name="question-circle"
            style={{fontSize: 25, textAlignVertical: 'center'}}
          />
          <Text style={styles.textProfile}>Bantuan</Text>
        </View>
      </View>
      <View style={styles.viewParentWithMarginBottom}>
        <View style={{flexDirection: 'row'}}>
          <Icon
            name="file-text"
            style={{fontSize: 25, textAlignVertical: 'center'}}
          />
          <Text style={styles.textProfile}>Syarat & Ketentuan</Text>
        </View>
      </View>
      <View style={styles.viewParentWithBorderBottom}>
        <View style={{flexDirection: 'row'}}>
          <Icon
            name="arrow-circle-o-left"
            style={{fontSize: 25, textAlignVertical: 'center'}}
          />
          <Text style={styles.textProfile}>Keluar</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewComponent: {
    flex: 1,
    flexDirection: 'column',
  },
  profileImg: {
    height: 60,
    width: 60,
    borderRadius: 40,
    marginVertical: 10,
  },
  viewPhotoName: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 20,
    alignContent: 'center',
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
  },
  viewTextName: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlignVertical: 'center',
    flex: 1,
    padding: 25,
  },
  viewParentWithMarginBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 20,
    height: 70,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: 5,
  },
  textProfile: {
    fontSize: 20,
    textAlignVertical: 'center',
    marginLeft: 15,
  },
  viewParentWithBorderBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 20,
    height: 70,
    alignContent: 'center',
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
  },
});

export default Index;
