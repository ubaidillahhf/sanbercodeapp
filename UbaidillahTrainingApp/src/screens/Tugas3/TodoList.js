import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const TodoList = () => {
  const [todoList, setTodoList] = useState();
  const [list, setList] = useState([]);

  const addList = () => {
    const today = new Date();
    const dateFormater =
      today.getDate() +
      '-' +
      parseInt(today.getMonth() + 1) +
      '-' +
      today.getFullYear() +
      ' ' +
      today.getHours() +
      ':' +
      today.getMinutes() +
      ':' +
      today.getSeconds();
    setList((list) => [
      ...list,
      {
        id: list.length + 1,
        listName: todoList,
        date: dateFormater,
      },
    ]);
  };

  const removeList = (idToRemove) => {
    const products = list.filter((prod) => prod.id !== idToRemove);
    setList(products);
  };

  return (
    <>
      <View style={styles.viewParentWithMarginBottom}>
        <View style={{backgroundColor: 'white', justifyContent: 'center'}}>
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => setTodoList(text)}
            placeholder="Input Here"
            value={todoList}
          />
        </View>
        <View style={styles.buttonPlus}>
          <TouchableOpacity onPress={() => addList()}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                fontSize: 20,
                color: 'white',
              }}>
              +
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.viewComponent}>
        <FlatList
          data={list}
          renderItem={({item}) => (
            <View style={styles.viewParentWithMarginBottom}>
              <View style={styles.viewList}>
                <Text style={{fontSize: 10}}>{item.date}</Text>
                <Text style={{fontSize: 14}}>{item.listName}</Text>
              </View>
              <View style={{justifyContent: 'center', marginRight: 15}}>
                <TouchableOpacity onPress={() => removeList(item.id)}>
                  <Icon
                    name="trash-o"
                    style={{fontSize: 25, textAlignVertical: 'center'}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}
          keyExtractor={(item) => item.id}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  viewComponent: {
    flex: 1,
    flexDirection: 'column',
    padding: 5,
  },
  viewParentWithMarginBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 10,
    height: 70,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: 5,
  },
  textInput: {
    height: 40,
    width: 310,
    borderColor: 'black',
  },
  buttonPlus: {
    width: 50,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#3A86FF',
    justifyContent: 'center',
    alignContent: 'center',
  },
  viewList: {
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 15,
  },
});

export default TodoList;
