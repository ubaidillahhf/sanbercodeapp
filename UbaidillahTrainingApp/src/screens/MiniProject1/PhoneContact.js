import React, {useState, useEffect, useContext} from 'react';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {PhoneContactContext} from './Index';
import imageDefaultContact from '../../assets/images/default-user.png';
const imageDefaultContactUri = Image.resolveAssetSource(imageDefaultContact)
  .uri;

const PhoneContact = (props) => {
  const [phoneContact, setPhoneContact] = useState();
  const [numberPhone, setNumberPhone] = useState();
  const {list, setList} = useContext(PhoneContactContext);

  const addList = () => {
    setList((list) => [
      ...list,
      {
        id: list.length + 1,
        listName: phoneContact,
        date: numberPhone,
      },
    ]);
  };

  const removeList = (idToRemove) => {
    const products = list.filter((prod) => prod.id !== idToRemove);
    setList(products);
  };

  return (
    <>
      <View style={styles.viewParent}>
        <View style={{backgroundColor: 'white', justifyContent: 'center'}}>
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => setPhoneContact(text)}
            placeholder="Input Name Contact Here"
            value={phoneContact}
            maxLength={86}
          />
        </View>
      </View>
      <View style={styles.viewParentWithMarginBottom}>
        <View style={{backgroundColor: 'white', justifyContent: 'center'}}>
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => setNumberPhone(text)}
            placeholder="Input Number Here"
            value={numberPhone}
            maxLength={86}
          />
        </View>
      </View>
      <View
        style={{
          alignContent: 'center',
          justifyContent: 'center',
          // flex: 1,
          flexDirection: 'row',
          // backgroundColor: 'red',
          marginBottom: 15,
        }}>
        <View style={styles.buttonPlus}>
          <TouchableOpacity onPress={() => addList()}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                fontSize: 20,
                color: 'white',
              }}>
              TAMBAH KONTAK
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.viewComponent}>
        <FlatList
          data={list}
          renderItem={({item}) => (
            <View style={styles.viewParentWithMarginBottom}>
              <Image
                source={{
                  uri: imageDefaultContactUri,
                }}
                style={styles.profileImg}
              />
              <View style={styles.viewList}>
                <Text style={{fontSize: 14, fontWeight: 'bold'}}>
                  Nama : {item.listName}
                </Text>
                <Text style={{fontSize: 12}}>No. HP : {item.date}</Text>
              </View>
              <View
                style={{
                  marginRight: 120,
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    marginRight: 20,
                  }}>
                  <TouchableOpacity>
                    <Icon
                      name="phone"
                      style={{fontSize: 25, textAlignVertical: 'center'}}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => removeList(item.id)}>
                    <Icon
                      name="trash-o"
                      style={{fontSize: 25, textAlignVertical: 'center'}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  viewComponent: {
    flex: 1,
    flexDirection: 'column',
    paddingRight: 5,
    paddingLeft: 5,
  },
  viewParent: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 10,
    height: 70,
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  viewParentWithMarginBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 10,
    height: 70,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: 10,
  },
  textInput: {
    height: 40,
    width: widthPercentageToDP('78%'),
    borderColor: 'black',
  },
  buttonPlus: {
    width: 350,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#3A86FF',
    justifyContent: 'center',
    alignContent: 'center',
  },
  viewList: {
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 15,
    width: widthPercentageToDP('60%'),
  },
  profileImg: {
    height: 40,
    width: 40,
    borderRadius: 40,
    marginVertical: 5,
  },
});

export default PhoneContact;
