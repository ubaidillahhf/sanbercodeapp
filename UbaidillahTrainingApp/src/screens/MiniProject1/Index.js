import React, {useState, createContext} from 'react';

const PhoneContactContext = createContext();

const ListProvider = (props) => {
  const [list, setList] = useState([]);

  const providerValue = {
    list,
    setList,
  };

  return (
    <PhoneContactContext.Provider value={providerValue}>
      {props.children}
    </PhoneContactContext.Provider>
  );
};

export {PhoneContactContext, ListProvider};
