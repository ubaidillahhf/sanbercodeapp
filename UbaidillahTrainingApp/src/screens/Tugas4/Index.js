import React, {useState, createContext} from 'react';

const TodoListContext = createContext();

const ListProvider = (props) => {
  const [list, setList] = useState([]);

  const providerValue = {
    list,
    setList,
  };

  return (
    <TodoListContext.Provider value={providerValue}>
      {props.children}
    </TodoListContext.Provider>
  );
};

export {TodoListContext, ListProvider};
