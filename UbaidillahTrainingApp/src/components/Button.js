import React from 'react';
import {Button} from 'react-native';

const Tombol = (props) => {
  return (
    <>
      <Button style={props.style} onPress={props.onPress} />
    </>
  );
};

export {Tombol};
