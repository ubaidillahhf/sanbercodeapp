import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import AppNavigation from './src/navigations/index';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';

var firebaseConfig = {
  apiKey: 'AIzaSyDupBMJBhOy6rvQJ92d4icDBn0AjLzHje8',
  authDomain: 'sanbercode-bfcfb.firebaseapp.com',
  databaseURL: 'https://sanbercode-bfcfb-default-rtdb.firebaseio.com/',
  projectId: 'sanbercode-bfcfb',
  storageBucket: 'sanbercode-bfcfb.appspot.com',
  messagingSenderId: '1016470096802',
  appId: '1:1016470096802:web:0be41a0fff16c3f2d980fb',
  measurementId: 'G-13TDYBYRP4',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App: () => React$Node = () => {
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init('f6302041-843a-4bb9-93d4-e5c072dd0ef6', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    //will unmount
    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  }, []);

  const onReceived = (notification) => {
    console.log('onReceived -> notification', notification);
  };

  const onOpened = (openResult) => {
    console.log('onOpened -> openResult', openResult);
  };

  const onIds = (device) => {
    console.log('onIds -> device', device);
  };
  return (
    <>
      <AppNavigation />
    </>
  );
};

export default App;
