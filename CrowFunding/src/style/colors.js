export const colors = {
  blue: '#3A86FF',
  white: '#FFFFFF',
  orange: '#F76300',
  blueDark: '#191970',
};
