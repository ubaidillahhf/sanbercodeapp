import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoidWJhaWRpbGxhaGhmIiwiYSI6ImNrajIyamVpZzI5Z2UzMHJ3Y2U1djU5NzcifQ.eySaPi8vdIhinSPcE3raLA',
);

const Help = () => {
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (err) {
        console.log(error);
      }
    };
    getLocation();
  }, []);

  const coordinates = [
    [107.58011, -6.890066],
    [106.819449, -6.218465],
    [110.365231, -7.795766],
  ];

  return (
    <View style={{flex: 1}}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coordinates.map((prop, key) => {
          return (
            <MapboxGL.PointAnnotation id="pointAnnotation" coordinate={prop}>
              <MapboxGL.Callout title="Desoku Jon" />
            </MapboxGL.PointAnnotation>
          );
        })}
      </MapboxGL.MapView>
      <View style={{flex: 1}}>
        <View style={styles.viewParentWithMarginBottom}>
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="wpexplorer"
              style={{fontSize: 25, textAlignVertical: 'center'}}
            />
            <Text style={styles.textProfile}>Jakarta, Bandung, Jogja</Text>
          </View>
        </View>
        <View style={styles.viewParentWithMarginBottom}>
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="wpexplorer"
              style={{fontSize: 25, textAlignVertical: 'center'}}
            />
            <Text style={styles.textProfile}>cscrowdfunding.com</Text>
          </View>
        </View>
        <View style={styles.viewParentWithMarginBottom}>
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="wpexplorer"
              style={{fontSize: 25, textAlignVertical: 'center'}}
            />
            <Text style={styles.textProfile}>(021)777-888</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Help;
