import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  StyleSheet,
  StatusBar,
} from 'react-native';
import Axios from 'axios';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../../api/index';
import {colors} from '../../style/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import {color} from 'react-native-reanimated';

const RegisterAccount = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const [namaLengkap, setNamaLengkap] = useState('');

  const signInWithGoogle = () => {
    alert('Under Development');
  };

  const signInWithFacebook = () => {
    alert('Under Development');
  };

  const onRegisterPress = () => {
    const data = {
      email,
      name: namaLengkap,
    };

    Axios.post(`${API}/auth/register`, data, {
      timeout: 20000,
    })
      .then((res) => {
        console.log('Register Account->res', res);
        navigation.navigate('InputOTP', {
          email: res.data.data.user.email,
        });
      })
      .catch((err) => {
        console.log('Register Account->err', err.response.data.errors.email[0]);
        if (err.response.data.errors.email.length !== 0) {
          Alert.alert(
            'Ops, Sorry 🙏',
            `${err.response.data.errors.email[0]}`,
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Login',
                onPress: () =>
                  navigation.navigate('Login', {
                    email: email,
                  }),
              },
            ],
            {cancelable: false},
          );
        } else {
          alert('Register Failed!');
        }
      });
  };

  return (
    <>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={colors.blue}
          barStyle="dark-content"></StatusBar>
        <View style={styles.imageContainer}>
          <Image
            source={require('../../assets/images/register.png')}
            style={styles.logo}
          />
        </View>
        <View style={styles.content}>
          <View style={styles.formContainer}>
            <Text>Username</Text>
            <TextInput
              value={email}
              underlineColorAndroid="#c6c6c6"
              placeholder="username or email"
              onChangeText={(email) => setEmail(email)}
            />
            <Text>Nama Lengkap</Text>
            <TextInput
              value={namaLengkap}
              underlineColorAndroid="#c6c6c6"
              placeholder="Nama Lengkap"
              onChangeText={(namaLengkap) => setNamaLengkap(namaLengkap)}
            />
          </View>
          <View style={styles.containerBtn}>
            <TouchableOpacity
              style={styles.btnLogin}
              onPress={() => onRegisterPress()}>
              <Text style={styles.btnTextLogin}>DAFTAR</Text>
            </TouchableOpacity>
            <View style={{marginBottom: 15}}>
              <Text style={{fontWeight: 'bold'}}>─────── ATAU VIA ───────</Text>
            </View>
            <View
              style={{marginTop: 10, marginBottom: 10, flexDirection: 'row'}}>
              {/* <GoogleSigninButton
                onPress={() => signInWithGoogle()}
                style={{width: '100%', height: 40}}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                style={{backgroundColor: 'rgba(0,0,0,0)'}}
              /> */}
              <TouchableOpacity
                style={styles.btnLoginFacebook}
                onPress={() => signInWithFacebook()}>
                <View
                  style={{
                    flexDirection: 'row',
                    // flex: 1,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingLeft: 1,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      width: 40,
                      height: 40,
                      // backgroundColor: 'red',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      name="facebook-square"
                      style={{
                        fontSize: 25,
                        textAlignVertical: 'center',
                        color: colors.blue,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 2,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{color: colors.blue}}>Facebook</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btnLoginGoogle}
                onPress={() => signInWithGoogle()}>
                <View
                  style={{
                    flexDirection: 'row',
                    // flex: 1,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingLeft: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      width: 40,
                      height: 40,
                      // backgroundColor: 'red',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      name="google"
                      style={{
                        fontSize: 25,
                        textAlignVertical: 'center',
                        color: colors.blue,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 2,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{color: colors.blue}}>Sign in</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Text>
              Sudah mempunyai akun?{' '}
              <Text
                style={{fontWeight: 'bold', color: colors.blue}}
                onPress={() => navigation.navigate('Login')}>
                Login
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  viewComponentEdit: {
    flex: 1,
    flexDirection: 'column',
  },
  container: {},
  content: {
    alignItems: 'center',
  },
  formContainer: {
    width: 350,
    marginBottom: 50,
  },
  imageContainer: {
    marginTop: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  logo: {
    width: 100,
    height: 100,
  },
  containerBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
  },
  btnLogin: {
    width: 350,
    height: 50,
    backgroundColor: colors.blue,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },

  btnTextLogin: {
    color: colors.white,
  },
  btnLoginGoogle: {
    width: 120,
    height: 55,
    backgroundColor: colors.white,
    borderColor: colors.blue,
    borderRadius: 5,
    borderWidth: 1,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
    marginLeft: 20,
  },
  btnLoginFacebook: {
    width: 120,
    height: 55,
    backgroundColor: colors.white,
    borderColor: colors.blue,
    borderRadius: 5,
    borderWidth: 1,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  // btnLoginFingerprint: {
  //   width: 305,
  //   height: 40,
  //   backgroundColor: colors.blueDark,
  //   marginBottom: 10,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   borderRadius: 2.5,
  //   shadowColor: 'rgba(0, 0, 0, 0.1)',
  //   shadowOpacity: 0.7,
  //   elevation: 6,
  //   shadowRadius: 20,
  //   shadowOffset: {width: 1, height: 13},
  // },
  // btnTextLoginFingerprint: {
  //   color: colors.white,
  //   fontWeight: 'bold',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
});

export default RegisterAccount;
