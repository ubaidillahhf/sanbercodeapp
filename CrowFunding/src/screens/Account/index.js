import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {API_GET_VENUE} from '../../api/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';

import imageDefaultUser from '../../assets/images/default-user.png';
const getImageDefaultUser = Image.resolveAssetSource(imageDefaultUser).uri;

const Account = ({navigation, route}) => {
  const [profilName, setProfilName] = useState('');
  const [profilePhoto, setProfilePhoto] = useState(getImageDefaultUser);
  const [profilEmail, setProfileEmail] = useState('');
  const [userInfo, setUserInfo] = useState(null);
  const [loginWith, setLoginWith] = useState('');

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  useEffect(() => {
    setProfilName(route.params?.name);
    setProfilePhoto(route.params?.photo);
  }, [route]);

  useEffect(() => {
    // Login Basic
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        return getProfile(token);
      } catch (error) {
        console.log(error);
      }
    }

    getToken();
    console.log('login bacic -> bro');
    setLoginWith('basic');

    // Login With Google
    getCurrentUser();
    setLoginWith('google');
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '1016470096802-15gtlbqdl7c1julv75sf1v4u4tk2985v.apps.googleusercontent.com',
    });
  };

  const getProfile = (token) => {
    Axios.get(`${API_GET_VENUE}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        setProfilName(res.data.data.profile.name);
        setProfilePhoto(
          `https://crowdfunding.sanberdev.com${res.data.data.profile.photo}`,
        );
        setProfileEmail(res.data.data.profile.email);
        console.log('PROFILE -> RES', res.data.data.profile.photo);
      })
      .catch((error) => {
        console.log('pofile->err', error);
      });
  };

  const getCurrentUser = async () => {
    const userInfo = await GoogleSignin.signInSilently().catch((error) => {
      console.log('Google Get Current User -> err', error);
    });
    console.log('getCurrentUser -> userInfo', userInfo);
    setUserInfo(userInfo ? userInfo : null);
    setProfilePhoto(userInfo && userInfo.user && userInfo.user.photo);
    setProfilName(userInfo && userInfo.user && userInfo.user.name);
  };

  const onLogoutPress = async () => {
    try {
      if (userInfo !== null) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }

      await AsyncStorage.removeItem('token');
      // navigation.navigate('Login');

      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } catch (error) {
      console.log('pofile->err', error);
    }
  };

  return (
    <>
      <View style={styles.viewComponent}>
        <View style={styles.viewPhotoName}>
          <Image
            source={{
              uri: profilePhoto + '?' + new Date(),
              cache: 'reload',
              headers: {Pragma: 'no-cache'},
            }}
            style={styles.profileImg}
          />
          <Text
            style={styles.viewTextName}
            onPress={() =>
              navigation.navigate('EditAccount', {
                name: profilName,
                email: profilEmail,
                photo: profilePhoto + '?' + new Date(),
                cache: 'reload',
                headers: {Pragma: 'no-cache'},
              })
            }>
            {profilName}
          </Text>
        </View>
        <View style={styles.viewParentWithMarginBottom}>
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="wpexplorer"
              style={{fontSize: 25, textAlignVertical: 'center'}}
            />
            <Text style={styles.textProfile}>Saldo</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={{textAlignVertical: 'center', fontWeight: 'bold'}}>
              Rp. 120.000.000
            </Text>
          </View>
        </View>
        <View style={styles.viewParentWithBorderBottom}>
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="gears"
              style={{fontSize: 25, textAlignVertical: 'center'}}
            />
            <Text style={styles.textProfile}>Pengaturan</Text>
          </View>
        </View>
        <View style={styles.viewParentWithBorderBottom}>
          <TouchableOpacity onPress={() => navigation.navigate('Help')}>
            <View style={{flexDirection: 'row'}}>
              <Icon
                name="question-circle"
                style={{fontSize: 25, textAlignVertical: 'center'}}
              />
              <Text style={styles.textProfile}>Bantuan</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.viewParentWithMarginBottom}>
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="file-text"
              style={{fontSize: 25, textAlignVertical: 'center'}}
            />
            <Text style={styles.textProfile}>Syarat & Ketentuan</Text>
          </View>
        </View>
        <View style={styles.viewParentWithBorderBottom}>
          <TouchableOpacity onPress={() => onLogoutPress()}>
            <View style={{flexDirection: 'row'}}>
              <Icon
                name="arrow-circle-o-left"
                style={{fontSize: 25, textAlignVertical: 'center'}}
              />
              <Text style={styles.textProfile}>Keluar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default Account;
