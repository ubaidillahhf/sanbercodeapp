import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  StyleSheet,
  StatusBar,
} from 'react-native';
import Axios from 'axios';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../../api/index';
import {colors} from '../../style/colors';
import Icon from 'react-native-vector-icons/Feather';

const ResetPassword = ({navigation, route}) => {
  const [token, setToken] = useState('');
  const [password, setPassword] = useState('');
  const [editable, setEditable] = useState(false);
  const [rePassword, setRePassword] = useState('');
  const [reEditable, setReEditable] = useState(false);
  const [email, setEmail] = useState('');

  useEffect(() => {
    setEmail(route.params?.email);
  }, []);

  const editData = () => {
    setEditable(!editable);
  };

  const editReData = () => {
    setReEditable(!reEditable);
  };

  const onSavePress = () => {};

  return (
    <>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={colors.blue}
          barStyle="dark-content"></StatusBar>
        <View style={styles.imageContainer}>
          <Image
            source={require('../../assets/images/reset-password.png')}
            style={styles.logo}
          />
        </View>
        <View style={styles.content}>
          <View style={styles.formContainer}>
            <Text>Masukkan Email</Text>
            <TextInput
              value={email}
              underlineColorAndroid="#c6c6c6"
              placeholder="Masukkan Email"
              onChangeText={(email) => setEmail(email)}
            />
          </View>
          <View style={styles.containerBtn}>
            <TouchableOpacity
              style={styles.btnLogin}
              onPress={() => onSavePress()}>
              <Text style={styles.btnTextLogin}>RESET PASSWORD</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  viewComponentEdit: {
    flex: 1,
    flexDirection: 'column',
  },
  container: {},
  content: {
    alignItems: 'center',
  },
  formContainer: {
    width: 350,
    marginBottom: 50,
  },
  imageContainer: {
    marginTop: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  logo: {
    width: 100,
    height: 100,
  },
  containerBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
  },
  btnLogin: {
    width: 350,
    height: 50,
    backgroundColor: colors.blue,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLogin: {
    color: colors.white,
  },
  btnLoginGoogle: {
    width: 350,
    height: 55,
    backgroundColor: colors.orange,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLoginFingerprint: {
    width: 305,
    height: 40,
    backgroundColor: colors.blueDark,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2.5,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLoginFingerprint: {
    color: colors.white,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ResetPassword;
