import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  StyleSheet,
  StatusBar,
} from 'react-native';
import Axios from 'axios';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../../api/index';
import {colors} from '../../style/colors';
import {GoogleSigninButton} from '@react-native-community/google-signin';
import OTPInputView from '@twotalltotems/react-native-otp-input';

const InputOTP = ({navigation, route}) => {
  const [token, setToken] = useState('');
  const [editable, setEditable] = useState(false);
  const [email, setEmail] = useState('');
  const [namaLengkap, setNamaLengkap] = useState('');
  const [OTP, setOTP] = useState(0);

  //   useEffect(() => {
  //     const getToken = async () => {
  //       try {
  //         const token = await AsyncStorage.getItem('token');
  //         if (token !== null) {
  //           setToken(token);
  //         }
  //       } catch (err) {
  //         console.log(err);
  //       }
  //     };
  //     getToken();
  //   }, []);

  const editData = () => {
    setEditable(!editable);
  };

  const onVerificationPress = () => {
    const data = {
      otp: OTP,
    };

    Axios.post(`${API}/auth/verification`, data, {
      timeout: 20000,
    })
      .then((res) => {
        console.log('Send Otp->res', res.data);
        // navigation.navigate('InputOTP', {
        //   email: res.data.data.user.email,
        // });
        if (res.data.response_code === '00') {
          navigation.navigate('SetupPassword', {
            name: route.params.name,
            email: route.params.email,
          });
        } else if (
          res.data.response_message ===
          'kode otp sudah tidak berlaku, silahkan generate ulang'
        ) {
          Alert.alert(
            'Kode OTP tidak berlaku!',
            `Silahkan klik kirim ulang`,
            [
              {
                text: 'OK',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
            ],
            {cancelable: false},
          );
        } else if (res.data.response_message === 'OTP Code tidak ditemukan') {
          Alert.alert(
            'OTP Salah!',
            `Silahkan Cek Kembali Email Anda`,
            [
              {
                text: 'OK',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
            ],
            {cancelable: false},
          );
        }
      })
      .catch((err) => {
        console.log('Send Otp->err', err.response.data.response_message);
      });
  };

  const regenerateOTP = () => {
    Axios.post(
      `${API}/auth/regenerate-otp`,
      {email: route.params.email},
      {
        timeout: 20000,
      },
    )
      .then((res) => {
        console.log('regenerate Otp->res', res.data);

        Alert.alert(
          'OTP dikirim ulang!',
          `Silahkan Cek Email ${route.params.email}`,
          [
            {
              text: 'OK',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
          ],
          {cancelable: false},
        );
      })
      .catch((err) => {
        console.log('Send Otp->err', err.response.data.response_message);
      });
  };

  return (
    <>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={colors.blue}
          barStyle="dark-content"></StatusBar>
        <View style={styles.imageContainer}>
          <Image
            source={require('../../assets/images/otp.png')}
            style={styles.logo}
          />
        </View>
        <View style={styles.content}>
          <View style={styles.formContainer}>
            <Text style={{fontWeight: 'bold', fontSize: 18}}>
              Perjalanan kebaikanmu dimulai di sini !
            </Text>
            <Text>Masukkan 6 digit kode yang kami kirimkan ke</Text>
            <Text style={{fontWeight: 'bold'}}>{route.params.email}</Text>
          </View>
          <OTPInputView
            pinCount={6}
            style={{
              width: '80%',
              height: 100,
              marginBottom: 40,
            }}
            autoFocusOnLoad
            onCodeFilled={(code) => {
              setOTP(code);
              console.log(`Code is ${code}, you are good to go!`);
            }}
          />
          <View style={styles.containerBtn}>
            <TouchableOpacity
              style={styles.btnLogin}
              onPress={() => onVerificationPress()}>
              <Text style={styles.btnTextLogin}>VERIFIKASI</Text>
            </TouchableOpacity>
          </View>
          <View style={{alignItems: 'center'}}>
            <Text>Belum menerima kode verifikasi? </Text>
            <View style={{marginBottom: 15}} />
            <Text
              style={{fontWeight: 'bold', color: colors.blue, fontSize: 16}}
              onPress={() => regenerateOTP()}>
              Kirim Ulang
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  viewComponentEdit: {
    flex: 1,
    flexDirection: 'column',
  },
  container: {},
  content: {
    alignItems: 'center',
  },
  formContainer: {
    width: 350,
    marginBottom: 20,
  },
  imageContainer: {
    marginTop: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  logo: {
    width: 100,
    height: 100,
  },
  containerBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
  },
  btnLogin: {
    width: 350,
    height: 50,
    backgroundColor: colors.blue,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLogin: {
    color: colors.white,
  },
  btnLoginGoogle: {
    width: 350,
    height: 55,
    backgroundColor: colors.orange,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLoginFingerprint: {
    width: 305,
    height: 40,
    backgroundColor: colors.blueDark,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2.5,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLoginFingerprint: {
    color: colors.white,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
  },
  //OTP
});

export default InputOTP;
