import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  viewComponent: {
    flex: 1,
    flexDirection: 'column',
  },
  profileImg: {
    height: 60,
    width: 60,
    borderRadius: 40,
    marginVertical: 10,
  },
  viewPhotoName: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 20,
    alignContent: 'center',
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
  },
  viewTextName: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlignVertical: 'center',
    flex: 1,
    padding: 25,
  },
  viewParentWithMarginBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 20,
    height: 70,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: 5,
  },
  textProfile: {
    fontSize: 20,
    textAlignVertical: 'center',
    marginLeft: 15,
  },
  viewParentWithBorderBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 20,
    height: 70,
    alignContent: 'center',
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
  },
  //EDIT ACCOUNT
  viewComponentEdit: {
    flex: 1,
    flexDirection: 'column',
  },
  viewEditCard: {
    flexDirection: 'column',
    backgroundColor: '#FFFFFF',
    padding: 20,
    height: 200,
    alignContent: 'center',
    marginBottom: 30,
  },
  buttonSaveEdit: {
    width: 320,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#3A86FF',
    justifyContent: 'center',
    alignContent: 'center',
  },
});

export default styles;
