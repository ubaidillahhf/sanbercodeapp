import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  StyleSheet,
  StatusBar,
  FlatList,
  Animated,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';

import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../../api/index';
import {colors} from '../../style/colors';
import Icon from 'react-native-vector-icons/Feather';

const Donasi = ({navigation, route}) => {
  const [list, setList] = useState([]);
  const [token, setToken] = useState('');

  const onSavePress = () => {};

  useEffect(() => {
    // Login Basic
    async function getToken() {
      try {
        const getToken = await AsyncStorage.getItem('token');
        setToken(getToken);
      } catch (error) {
        console.log(error);
      }
    }

    getToken();
  }, []);

  useEffect(() => {
    getDonationList();
  }, [token]);

  const getDonationList = () => {
    Axios.get(`${API}/donasi/daftar-donasi`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        console.log('login->res', res.data.data.donasi);
        setList(res.data.data.donasi);
      })
      .catch((error) => {
        console.log('login->error', error.response);
      });
  };

  return (
    <>
      <View style={styles.viewComponent}>
        <FlatList
          data={list}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('DetailDonasi', {
                  photo: `https://crowdfunding.sanberdev.com${item?.photo}`,
                  penggalang: item.user?.name,
                  judul: item?.title,
                  deskripsi: item?.description,
                  donasi: item?.donation,
                  id: item?.id,
                })
              }>
              <View style={styles.viewParentWithMarginBottom}>
                <View style={styles.viewList}>
                  <Image
                    source={{
                      uri: `https://crowdfunding.sanberdev.com${item?.photo}`,
                    }}
                    style={{width: 150, height: 100, borderRadius: 10}}
                  />
                </View>
                <View
                  style={{
                    // justifyContent: 'center',
                    marginRight: 15,
                    flexDirection: 'column',
                    flex: 1,
                  }}>
                  <Text style={{fontSize: 14, fontWeight: 'bold'}}>
                    {item?.title}
                  </Text>
                  <Text style={{fontSize: 14}}>{item.user?.name}</Text>
                  <View style={styles.progressBar}>
                    <Animated.View style={styles.absoluteFill} />
                  </View>
                  <Text style={{fontSize: 14, marginTop: 10}}>
                    Dana yang dibutuhkan
                  </Text>
                  <TextInputMask
                    style={{fontWeight: 'bold', fontSize: 18}}
                    type={'money'}
                    options={{
                      precision: 2,
                      separator: ',',
                      delimiter: '.',
                      unit: 'Rp. ',
                    }}
                    value={item?.donation}
                    onChangeText={(text) => {
                      this.setState({
                        simple: text,
                      });
                    }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item) => item.id}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  viewComponent: {
    flex: 1,
    flexDirection: 'column',
    padding: 5,
  },
  viewParentWithMarginBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 10,
    height: 150,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: 5,
  },
  textInput: {
    height: 40,
    width: 310,
    borderColor: 'black',
  },
  buttonPlus: {
    width: 50,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#3A86FF',
    justifyContent: 'center',
    alignContent: 'center',
  },
  viewList: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 15,
  },
  progressBar: {
    height: 10,
    width: '100%',
    backgroundColor: 'white',
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 5,
  },
  absoluteFill: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: '#3A86FF',
    width: '50%',
  },
});

export default Donasi;
