import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  StyleSheet,
  StatusBar,
  ScrollView,
} from 'react-native';
import {API} from '../../api/index';
import {colors} from '../../style/colors';
import {TextInputMask} from 'react-native-masked-text';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';

const DetailDonasi = ({navigation, route}) => {
  const [donasi, setDonasi] = useState(0);
  const [token, setToken] = useState('');
  const [data, setData] = useState(null);
  const [donasi1, setDonasi1] = useState(true);
  const [donasi2, setDonasi2] = useState(true);
  const [donasi3, setDonasi3] = useState(true);
  const [donasi4, setDonasi4] = useState(true);
  const [donasi5, setDonasi5] = useState(true);
  const [donasi6, setDonasi6] = useState(true);

  const getProfile = (token) => {
    Axios.get(`${API}/profile/get-profile`, {
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        const data = res.data.data.profile;
        setData(data);
        console.log('detail donasi -> res', data);
      })
      .catch((err) => {
        console.log('detail donasi -> err', err.response);
      });
  };

  const onLanjutkanPress = () => {
    const time = new Date().getTime();
    const body = {
      transaction_details: {
        order_id: `Donasi-${time}`,
        gross_amount: donasi,
        donation_id: route.params.id,
      },
      customer_detail: {
        first_name: data.name,
        email: data.email,
      },
    };
    Axios.post(`${API}/donasi/generate-midtrans`, body, {
      timeout: 2000,
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        navigation.replace('Payment', res.data.data);
        console.log('onPayPress -> res', res.data.data.midtrans.redirect_url);
      })
      .catch((err) => {
        console.log('onPayPress -> err', err.response);
      });
  };

  useEffect(() => {
    sheetRef.current.snapTo(1);
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) {
          setToken(token);
          getProfile(token);
        }
      } catch (err) {
        console.log(err);
      }
    };
    getToken();
  }, []);

  const selectDonasi = (selected) => {
    if (selected === 1) {
      setDonasi1(!donasi1);
      setDonasi2(true);
      setDonasi3(true);
      setDonasi4(true);
      setDonasi5(true);
      setDonasi6(true);
      setDonasi(10000);
    } else if (selected === 2) {
      setDonasi1(true);
      setDonasi2(!donasi2);
      setDonasi3(true);
      setDonasi4(true);
      setDonasi5(true);
      setDonasi6(true);
      setDonasi(20000);
    } else if (selected === 3) {
      setDonasi1(true);
      setDonasi2(true);
      setDonasi3(!donasi3);
      setDonasi4(true);
      setDonasi5(true);
      setDonasi6(true);
      setDonasi(50000);
    } else if (selected === 4) {
      setDonasi1(true);
      setDonasi2(true);
      setDonasi3(true);
      setDonasi4(!donasi4);
      setDonasi5(true);
      setDonasi6(true);
      setDonasi(100000);
    } else if (selected === 5) {
      setDonasi1(true);
      setDonasi2(true);
      setDonasi3(true);
      setDonasi4(true);
      setDonasi5(!donasi5);
      setDonasi6(true);
      setDonasi(200000);
    } else {
      setDonasi1(true);
      setDonasi2(true);
      setDonasi3(true);
      setDonasi4(true);
      setDonasi5(true);
      setDonasi6(!donasi6);
      setDonasi(500000);
    }
  };

  const renderContent = () => (
    <View
      style={{
        backgroundColor: colors.blue,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        padding: 16,
        // paddingTop: 30,
        height: 400,
      }}>
      <View
        style={{
          marginBottom: 15,
          alignItems: 'center',
        }}>
        <View
          style={{
            width: 100,
            height: 4,
            backgroundColor: 'white',
            borderRadius: 4,
          }}></View>
        {/* <Text style={{fontWeight: 'bold', color: 'white'}}>────────</Text> */}
      </View>
      <Text style={{color: 'white'}}>ISI NOMINAL :</Text>
      <TextInputMask
        style={{
          fontWeight: 'bold',
          fontSize: 18,
          color: 'white',
          marginBottom: 15,
        }}
        type={'money'}
        options={{
          precision: 0,
          separator: ',',
          delimiter: '.',
          unit: 'Rp. ',
        }}
        value={donasi}
        onChangeText={(amount) => {
          setDonasi(amount);
          setDonasi1(true);
          setDonasi2(true);
          setDonasi3(true);
          setDonasi4(true);
          setDonasi5(true);
          setDonasi6(true);
        }}
      />
      <View
        style={{
          // flex: 1,
          marginBottom: 15,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            height: 50,
            width: 100,
            backgroundColor: donasi1 ? 'white' : '#3CBD9F',
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOpacity: 0.7,
            elevation: 6,
            shadowRadius: 20,
            shadowOffset: {width: 1, height: 13},
          }}>
          <TouchableOpacity onPress={() => selectDonasi(1)}>
            <Text
              style={{fontWeight: 'bold', color: donasi1 ? 'black' : 'white'}}>
              Rp. 10.000
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 50,
            width: 100,
            backgroundColor: donasi2 ? 'white' : '#3CBD9F',
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOpacity: 0.7,
            elevation: 6,
            shadowRadius: 20,
            shadowOffset: {width: 1, height: 13},
          }}>
          <TouchableOpacity onPress={() => selectDonasi(2)}>
            <Text
              style={{fontWeight: 'bold', color: donasi2 ? 'black' : 'white'}}>
              Rp. 20.000
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 50,
            width: 100,
            backgroundColor: donasi3 ? 'white' : '#3CBD9F',
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOpacity: 0.7,
            elevation: 6,
            shadowRadius: 20,
            shadowOffset: {width: 1, height: 13},
          }}>
          <TouchableOpacity onPress={() => selectDonasi(3)}>
            <Text
              style={{fontWeight: 'bold', color: donasi3 ? 'black' : 'white'}}>
              Rp. 50.000
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          // flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 15,
        }}>
        <View
          style={{
            height: 50,
            width: 100,
            backgroundColor: donasi4 ? 'white' : '#3CBD9F',
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOpacity: 0.7,
            elevation: 6,
            shadowRadius: 20,
            shadowOffset: {width: 1, height: 13},
          }}>
          <TouchableOpacity onPress={() => selectDonasi(4)}>
            <Text
              style={{fontWeight: 'bold', color: donasi4 ? 'black' : 'white'}}>
              Rp. 100.000
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 50,
            width: 100,
            backgroundColor: donasi5 ? 'white' : '#3CBD9F',
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOpacity: 0.7,
            elevation: 6,
            shadowRadius: 20,
            shadowOffset: {width: 1, height: 13},
          }}>
          <TouchableOpacity onPress={() => selectDonasi(5)}>
            <Text
              style={{fontWeight: 'bold', color: donasi5 ? 'black' : 'white'}}>
              Rp. 200.000
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 50,
            width: 100,
            backgroundColor: donasi6 ? 'white' : '#3CBD9F',
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOpacity: 0.7,
            elevation: 6,
            shadowRadius: 20,
            shadowOffset: {width: 1, height: 13},
          }}>
          <TouchableOpacity onPress={() => selectDonasi(6)}>
            <Text
              style={{fontWeight: 'bold', color: donasi6 ? 'black' : 'white'}}>
              Rp. 500.000
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          style={{
            width: 350,
            height: 50,
            backgroundColor: 'white',
            marginTop: 20,
            borderRadius: 4,
            justifyContent: 'center',
            alignItems: 'center',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOpacity: 0.7,
            elevation: 6,
            shadowRadius: 20,
            shadowOffset: {width: 1, height: 13},
          }}
          onPress={() => onLanjutkanPress()}>
          <Text style={{color: colors.blue, fontWeight: 'bold'}}>
            LANJUTKAN
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  const sheetRef = React.useRef(null);

  return (
    <>
      <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
        <View style={styles.container}>
          <StatusBar
            backgroundColor={colors.blue}
            barStyle="dark-content"></StatusBar>
          <View style={styles.imageContainer}>
            <Image source={{uri: route.params.photo}} style={styles.logo} />
          </View>
          <View style={styles.content}>
            <View style={styles.formContainer}>
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                {route.params?.judul}
              </Text>
              <Text style={{fontSize: 16}}>Dana yang dibutuhkan :</Text>
              <TextInputMask
                style={{fontWeight: 'bold', fontSize: 18, color: colors.blue}}
                editable={false}
                type={'money'}
                options={{
                  precision: 2,
                  separator: ',',
                  delimiter: '.',
                  unit: 'Rp. ',
                }}
                value={route.params?.donasi}
                onChangeText={(text) => {
                  this.setState({
                    simple: text,
                  });
                }}
              />
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Deskripsi :
              </Text>
              <Text style={{fontSize: 14}}>{route.params?.deskripsi}</Text>
            </View>
            <View style={styles.containerBtn}>
              <TouchableOpacity
                style={styles.btnLogin}
                onPress={() => sheetRef.current.snapTo(0)}>
                <Text style={styles.btnTextLogin}>DONASI SEKARANG</Text>
              </TouchableOpacity>
            </View>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                marginTop: 20,
                marginBottom: 20,
              }}>
              Informasi Penggalang Dana :
            </Text>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                // backgroundColor: 'red',
                borderColor: colors.blue,
                borderRadius: 20,
                borderWidth: 2,
                height: 180,
                width: '80%',
              }}>
              <Text style={{marginBottom: 30}}>Penggalang Dana :</Text>
              <Image
                source={require('../../assets/images/default-user.png')}
                style={{height: 50, width: 50, marginBottom: 10}}
              />
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontWeight: 'bold'}}>
                  {route.params?.penggalang}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <BottomSheet
        ref={sheetRef}
        snapPoints={[400, 0, 0]}
        renderContent={renderContent}
        // enabledBottomClamp={true}
        enabledInnerScrolling={false}
        enabledContentTapInteraction={false}
      />
    </>
  );
};

const styles = StyleSheet.create({
  viewComponentEdit: {
    flex: 1,
    flexDirection: 'column',
  },
  container: {},
  content: {
    alignItems: 'center',
  },
  formContainer: {
    width: 350,
    marginTop: 20,
  },
  imageContainer: {
    height: 200,
    // backgroundColor: 'red',
    // justifyContent: 'center',
    alignItems: 'center',

    marginBottom: 10,
  },
  logo: {
    width: '100%',
    height: 200,
  },
  containerBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    // marginBottom: 40,
  },
  btnLogin: {
    width: 350,
    height: 50,
    backgroundColor: colors.blue,
    marginTop: 20,
    // marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLogin: {
    color: colors.white,
  },
  btnLoginGoogle: {
    width: 350,
    height: 55,
    backgroundColor: colors.orange,
    // marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLoginFingerprint: {
    width: 305,
    height: 40,
    backgroundColor: colors.blueDark,
    // marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2.5,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLoginFingerprint: {
    color: colors.white,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default DetailDonasi;
