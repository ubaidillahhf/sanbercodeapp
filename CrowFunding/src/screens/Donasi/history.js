import React, {useState, useRef, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, FlatList} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';

import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../../api/index';

const History = ({navigation, route}) => {
  const [list, setList] = useState([]);
  const [token, setToken] = useState('');

  useEffect(() => {
    // Login Basic
    async function getToken() {
      try {
        const getToken = await AsyncStorage.getItem('token');
        setToken(getToken);
      } catch (error) {
        console.log(error);
      }
    }

    getToken();
  }, []);

  useEffect(() => {
    getDonationList();
  }, [token]);

  const getDonationList = () => {
    Axios.get(`${API}/donasi/riwayat-transaksi`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        console.log('login->res', res);
        setList(res.data.data.riwayat_transaksi);
      })
      .catch((error) => {
        console.log('login->error', error.response);
      });
  };

  return (
    <>
      <View style={styles.viewComponent}>
        <FlatList
          data={list}
          renderItem={({item}) => (
            <TouchableOpacity>
              <View style={styles.viewParentWithMarginBottom}>
                <View
                  style={{
                    // justifyContent: 'center',
                    marginRight: 15,
                    flexDirection: 'column',
                    flex: 1,
                  }}>
                  <TextInputMask
                    style={{fontWeight: 'bold', fontSize: 18}}
                    type={'money'}
                    options={{
                      precision: 2,
                      separator: ',',
                      delimiter: '.',
                      unit: 'Rp. ',
                    }}
                    value={item?.amount}
                    onChangeText={(text) => {
                      this.setState({
                        simple: text,
                      });
                    }}
                  />
                  <Text style={{fontSize: 14}}>
                    Donation ID : {item?.order_id}
                  </Text>
                  <Text style={{fontSize: 14}}>
                    Tanggal Transaksi : {item?.created_at}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item) => item.id}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  viewComponent: {
    flex: 1,
    flexDirection: 'column',
    padding: 5,
  },
  viewParentWithMarginBottom: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 10,
    height: 120,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: 5,
  },
  textInput: {
    height: 40,
    width: 310,
    borderColor: 'black',
  },
  buttonPlus: {
    width: 50,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#3A86FF',
    justifyContent: 'center',
    alignContent: 'center',
  },
  viewList: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 15,
  },
  progressBar: {
    height: 10,
    width: '100%',
    backgroundColor: 'white',
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 5,
  },
  absoluteFill: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: '#3A86FF',
    width: '50%',
  },
});

export default History;
