import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Modal,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import styles from './style';

import imageDefaultUser from '../../assets/images/default-user.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
const getImageDefaultUser = Image.resolveAssetSource(imageDefaultUser).uri;
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';
import {API} from '../../api/index';

const CreateDonation = ({navigation, route}) => {
  let input = useRef(null);
  let camera = useRef(null);
  const [token, setToken] = useState('');
  const [judul, setJudul] = useState('');
  const [deskripsi, setDeskripsi] = useState('');
  const [danaDibutuhkan, setDanaDibutuhkan] = useState('');
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState({uri: null});
  const [editable, setEditable] = useState(false);

  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setPhoto(data);
      setIsVisible(false);
      console.log('editAccount ->  data', data);
    }
  };

  useEffect(() => {
    // setPhoto({uri: route.params.photo});
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) {
          setToken(token);
        }
      } catch (err) {
        console.log(err);
      }
    };
    getToken();
  }, []);

  const editData = () => {
    setEditable(!editable);
  };

  const onSavePress = () => {
    console.log('editAccount->Photo Uri', photo.uri);
    const formData = new FormData();
    formData.append('title', judul);
    formData.append('description', deskripsi);
    formData.append('donation', danaDibutuhkan);
    formData.append('photo', {
      uri: photo.uri,
      name: 'photo.jpg',
      type: 'image/jpg',
    });
    Axios.post(`${API}/donasi/tambah-donasi`, formData, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        console.log('editAccount->resE', res);
        if (res.status === 200) {
          alert('Donasi Baru berhasil ditambahkan');
        }
      })
      .catch((err) => {
        console.log('editAccount->err', err);
        alert('Error Tambah Donasi!');
      });
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera style={{flex: 1}} type={type} ref={camera}>
            <View style={{flexDirection: 'row', marginTop: '180%'}}>
              <View
                style={{
                  marginLeft: 100,
                  width: 60,
                  height: 60,
                  borderRadius: 60,
                  backgroundColor: 'white',
                }}>
                <TouchableOpacity
                  onPress={() => toggleCamera()}
                  style={{
                    marginTop: 15,
                    alignItems: 'center',
                  }}>
                  <MaterialCommunity name="rotate-3d-variant" size={30} />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  marginLeft: 60,
                  marginBottom: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: 60,
                  height: 60,
                  backgroundColor: 'white',
                  borderRadius: 60,
                }}>
                <TouchableOpacity
                  onPress={() => takePicture()}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon name="camera" size={30} />
                </TouchableOpacity>
              </View>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <View style={styles.viewComponentEdit}>
        {photo.uri ? (
          <>
            <View>
              <Image
                source={{uri: photo.uri}}
                style={{
                  flexDirection: 'row',
                  padding: 20,
                  alignContent: 'center',
                  justifyContent: 'center',
                  height: 250,
                }}
              />

              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => setIsVisible(true)}
                style={{
                  backgroundColor: 'grey',
                  alignContent: 'center',
                  justifyContent: 'center',
                  height: 70,
                  width: 70,
                }}>
                <Icon
                  name="camera"
                  size={25}
                  color={'white'}
                  style={{marginLeft: 20}}
                />
              </TouchableOpacity>
            </View>
          </>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'grey',
              padding: 20,
              alignContent: 'center',
              justifyContent: 'center',
              borderBottomColor: '#F2F2F2',
              borderBottomWidth: 1,
            }}>
            <View
              style={{
                alignContent: 'center',
                justifyContent: 'center',
                height: 150,
              }}>
              <Image
                source={{uri: photo.uri}}
                style={{
                  height: 70,
                  width: 70,
                  borderRadius: 70,
                  marginVertical: 8,
                }}
              />

              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => setIsVisible(true)}
                style={{
                  backgroundColor: 'grey',
                  alignContent: 'center',
                  justifyContent: 'center',
                  marginTop: -56,
                  marginLeft: 20,
                }}>
                <Icon
                  name="camera"
                  size={15}
                  color={'white'}
                  style={{marginLeft: 7}}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
        <View style={styles.viewEditCard}>
          <View style={{flexDirection: 'column', marginBottom: 10}}>
            <Text style={{fontSize: 20, fontWeight: 'bold', marginLeft: 15}}>
              Judul
            </Text>
            <View style={{marginLeft: 15}}>
              <View
                style={{
                  backgroundColor: 'white',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <TextInput
                  ref={input}
                  style={{fontSize: 16}}
                  onChangeText={(text) => setJudul(text)}
                  placeholder="Judul"
                  value={judul}
                  //   maxLength={86}
                />
              </View>
            </View>
          </View>

          <View style={{flexDirection: 'column'}}>
            <Text style={{fontSize: 20, fontWeight: 'bold', marginLeft: 15}}>
              Deskripsi
            </Text>
            <View style={{marginLeft: 15}}>
              <View
                style={{
                  backgroundColor: 'white',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <TextInput
                  ref={input}
                  style={{fontSize: 16}}
                  onChangeText={(text) => setDeskripsi(text)}
                  placeholder="Deskripsi"
                  value={deskripsi}
                  //   maxLength={86}
                />
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'column'}}>
            <Text style={{fontSize: 20, fontWeight: 'bold', marginLeft: 15}}>
              Dana yang dibutuhkan
            </Text>
            <View style={{marginLeft: 15}}>
              <View
                style={{
                  backgroundColor: 'white',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <TextInput
                  ref={input}
                  style={{fontSize: 16}}
                  onChangeText={(text) => setDanaDibutuhkan(text)}
                  placeholder="Rp. 0"
                  value={danaDibutuhkan}
                  maxLength={86}
                />
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            alignContent: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
          <View style={styles.buttonSaveEdit}>
            <TouchableOpacity onPress={() => onSavePress()}>
              <Text
                style={{
                  textAlign: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                BUAT
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {renderCamera()}
      </View>
    </>
  );
};

export default CreateDonation;
