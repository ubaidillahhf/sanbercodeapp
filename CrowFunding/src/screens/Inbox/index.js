import React, {useEffect, useState} from 'react';
import {View, Text, StatusBar} from 'react-native';
import {colors} from '../../style/colors';
import Axios from 'axios';
import {API, URI} from '../../api';
import {GiftedChat} from 'react-native-gifted-chat';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Inbox = () => {
  const [user, setUser] = useState({});
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) {
          return getProfile(token);
        }
      } catch (err) {
        console.log('balabala', err);
      }
    };
    getToken();
    // getProfile();
    onRef();

    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getProfile = (token) => {
    Axios.get(`${API}/profile/get-profile`, {
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        console.log('Account -> res', res);
        const data = res.data.data.profile;
        setUser(data);
      })
      .catch((err) => {
        console.log('Account -> err', err);
      });
  };

  const onRef = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((previosMessages) =>
          GiftedChat.append(previosMessages, value),
        );
      });
  };

  const onSend = (messages = []) => {
    console.log('msg', messages);
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
      <GiftedChat
        messages={messages}
        onSend={(messages) => onSend(messages)}
        user={{
          _id: user.id,
          name: user.name,
          avatar: `${URI}${user.photo}`,
        }}
        showUserAvatar
      />
    </View>
  );
};

export default Inbox;
