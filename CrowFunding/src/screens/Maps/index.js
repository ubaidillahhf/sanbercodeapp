import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoidWJhaWRpbGxhaGhmIiwiYSI6ImNrajIyamVpZzI5Z2UzMHJ3Y2U1djU5NzcifQ.eySaPi8vdIhinSPcE3raLA',
);

const Maps = () => {
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (err) {
        console.log(error);
      }
    };
    getLocation();
  }, []);
  return (
    <View style={{flex: 1}}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        <MapboxGL.PointAnnotation
          id="pointAnnotation"
          coordinate={[112.697346, -7.5131932]}>
          <MapboxGL.Callout title="Desoku Jon" />
        </MapboxGL.PointAnnotation>
      </MapboxGL.MapView>
    </View>
  );
};

export default Maps;
