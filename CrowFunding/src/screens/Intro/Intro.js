import React from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import styles from './style';
import {colors} from '../../style/colors';
// import {Button} from '../../components/Button';
//import module  react native app intro slider
import AppIntroSlider from 'react-native-app-intro-slider';

// data yang akan kita tampilkan sebagai onboarding aplikasi
const data = [
  {
    id: 1,
    image: require('../../assets/images/Online-Education-1.png'),
    description: 'Easy Donation',
  },
  {
    id: 2,
    image: require('../../assets/images/Online-Education-2.png'),
    description: 'Get Idea About Donation',
  },
  {
    id: 3,
    image: require('../../assets/images/Online-Education-3.png'),
    description: 'Donation in 1 Place',
  },
];

const Intro = ({navigation}) => {
  //tampilan onboarding yang ditampilkan dalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listContent}>
          <Image
            source={item.image}
            style={styles.imgList}
            resizeMethod="auto"
            resizeMode="contain"
          />
        </View>
        <Text style={styles.textList}>{item.description}</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
        <View style={styles.textLogoContainer}>
          <Text style={styles.textLogo}>CrowdFunding</Text>
        </View>
        <View style={styles.slider}>
          {/* contoh menggunakan component react native app intro slider */}
          <AppIntroSlider
            data={data} //masukan data yang akan ditampilkan menjadi onBoarding, dia bernilai array
            renderItem={renderItem} // untuk menampilkan onBoarding dar data array
            renderNextButton={() => null}
            renderDoneButton={() => null}
            activeDotStyle={styles.activeDotStyle}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity
            style={styles.btnLogin}
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.btnTextLogin}>MASUK</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnRegister}
            onPress={() => navigation.navigate('RegisterAccount')}>
            <Text style={styles.btnTextRegister}>DAFTAR</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Intro;
