import {StyleSheet} from 'react-native';
import {colors} from '../../style/colors';

const styles = StyleSheet.create({
  listContainer: {
    // flex: 1,
    // justifyContent: 'center',
    // alignContent: 'center',
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  listContent: {
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 150,
    marginBottom: 50,
  },
  imgList: {
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#3A86FF',
  },
  textLogoContainer: {
    marginTop: 70,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },
  textLogo: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  slider: {
    flex: 3,
    flexDirection: 'row',
    // backgroundColor: 'yellow',
    justifyContent: 'center',
    alignItems: 'center',
    // marginBottom: 10,
  },
  activeDotStyle: {
    backgroundColor: colors.white,
    width: 20,
  },
  btnContainer: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'center',
  },
  btnLogin: {
    width: 350,
    height: 50,
    backgroundColor: colors.white,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnTextLogin: {
    color: colors.blue,
    // fontSize: 20,
    fontWeight: 'bold',
  },
  btnRegister: {
    width: 350,
    height: 50,
    backgroundColor: colors.blue,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: colors.white,
    borderWidth: 2,
  },
  btnTextRegister: {
    color: colors.white,
  },
  textList: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 20,
    // marginBottom: 70,
  },
});

export default styles;
