import {StyleSheet} from 'react-native';
import {color} from 'react-native-reanimated';
import {colors} from '../../style/colors';

const styles = StyleSheet.create({
  container: {},
  content: {
    alignItems: 'center',
  },
  formContainer: {
    width: 350,
    marginBottom: 50,
  },
  imageContainer: {
    marginTop: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  logo: {
    width: 100,
    height: 100,
  },
  containerBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
  },
  btnLogin: {
    width: 350,
    height: 50,
    backgroundColor: colors.blue,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLogin: {
    color: colors.white,
  },
  btnLoginGoogle: {
    width: 350,
    height: 55,
    backgroundColor: colors.orange,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLoginFingerprint: {
    width: 305,
    height: 40,
    backgroundColor: colors.blueDark,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2.5,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.7,
    elevation: 6,
    shadowRadius: 20,
    shadowOffset: {width: 1, height: 13},
  },
  btnTextLoginFingerprint: {
    color: colors.white,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
