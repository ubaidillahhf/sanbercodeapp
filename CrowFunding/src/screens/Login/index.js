import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../../api/index';
import auth from '@react-native-firebase/auth';
import {CommonActions} from '@react-navigation/native';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import styles from './style';
import {colors} from '../../style/colors';
import TouchID from 'react-native-touch-id';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// Config SignIn With Fingerprint
const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'cancel',
};

const Login = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const saveToken = async (token) => {
    try {
      if (token !== null) {
        console.log(`TOKENNYA = ${token}`);
        await AsyncStorage.setItem('token', token);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  useEffect(() => {
    setEmail(route.params?.email);
    setPassword(route.params?.password);
  }, [route.params?.email]);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '1016470096802-15gtlbqdl7c1julv75sf1v4u4tk2985v.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('SignInWithGoogle -> idToken', idToken);
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      navigation.navigate('Home');
    } catch (error) {
      Alert.alert(
        'Error',
        "Can't Login With Google",
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
      console.log('SignInWithGoogle -> error', error);
    }
  };

  const signInWithFingerPrint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        // alert('Authentication Success');
        navigation.navigate('Home');
      })
      .catch((error) => {
        alert('Authentication Failed');
      });
  };

  const onLoginPress = () => {
    let data = {
      email,
      password,
    };
    Axios.post(`${API}/auth/login`, data, {
      timeout: 20000,
    })
      .then((res) => {
        saveToken(res.data.data.token);
        // navigation.navigate('Home');
        navigation.reset({
          index: 0,
          routes: [{name: 'Home'}],
        });
      })
      .catch((error) => {
        console.log('login->error', error.response.data.error);
        if (error.response.data.error === 'Unauthorized') {
          alert('Email/Password Salah!');
        } else {
          alert(error.response.data.message);
        }
      });
  };

  return (
    <>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={colors.blue}
          barStyle="dark-content"></StatusBar>
        <View style={styles.imageContainer}>
          <Image
            source={require('../../assets/images/log-in.png')}
            style={styles.logo}
          />
        </View>
        <View style={styles.content}>
          <View style={styles.formContainer}>
            <Text>Email</Text>
            <TextInput
              value={email}
              underlineColorAndroid="#c6c6c6"
              placeholder="Masukkan Email"
              onChangeText={(email) => setEmail(email)}
            />
            <Text>Password</Text>
            <TextInput
              secureTextEntry
              value={password}
              underlineColorAndroid="#c6c6c6"
              placeholder="Masukkan Password"
              onChangeText={(password) => setPassword(password)}
            />
          </View>
          <View style={styles.containerBtn}>
            <TouchableOpacity
              style={styles.btnLogin}
              onPress={() => onLoginPress()}>
              <Text style={styles.btnTextLogin}>MASUK</Text>
            </TouchableOpacity>
            <View style={{marginBottom: 15}}>
              <Text style={{fontWeight: 'bold'}}>
                ──────────── OR ────────────
              </Text>
            </View>
            {/* <TouchableOpacity style={styles.btnLoginGoogle}>
              <Text style={styles.btnTextLogin}>LOGIN WITH GOOGLE</Text>
            </TouchableOpacity> */}
            <View style={{marginTop: 10, marginBottom: 10}}>
              <GoogleSigninButton
                onPress={() => signInWithGoogle()}
                style={{width: '100%', height: 40}}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                style={{backgroundColor: 'rgba(0,0,0,0)'}}
              />
            </View>
            <TouchableOpacity
              style={styles.btnLoginFingerprint}
              onPress={() => signInWithFingerPrint()}>
              <View
                style={{
                  flexDirection: 'row',
                  // flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flex: 1,
                    width: 40,
                    height: 40,
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="fingerprint"
                    style={{fontSize: 25, textAlignVertical: 'center'}}
                  />
                </View>
                <View
                  style={{
                    flex: 6,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.btnTextLoginFingerprint}>
                    Sign in with Fingerprint
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{alignItems: 'center'}}>
            <Text>
              Belum mempunyai akun ?{' '}
              <Text
                style={{fontWeight: 'bold', color: colors.blue}}
                onPress={() => navigation.navigate('RegisterAccount')}>
                Buat Akun
              </Text>
            </Text>
            <View style={{marginBottom: 10}}></View>
            <Text>
              Lupa Password ?{' '}
              <Text
                style={{fontWeight: 'bold', color: colors.blue}}
                onPress={() => navigation.navigate('ResetPassword', {email})}>
                Reset Password
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

export default Login;
