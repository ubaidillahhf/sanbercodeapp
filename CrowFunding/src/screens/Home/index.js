import React, {useState} from 'react';
import {
  View,
  TextInput,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';

const Home = ({navigation, route}) => {
  const [images, setImages] = useState([
    require('../../assets/images/baner1.png'),
    require('../../assets/images/baner2.jpg'),
    require('../../assets/images/baner3.jpg'),
  ]);
  return (
    <>
      <View style={{flex: 1}}>
        <View style={{backgroundColor: '#3A86FF', height: 70}}>
          <View
            style={{
              marginHorizontal: 17,
              //   marginLeft: 20,
              flexDirection: 'row',
              paddingTop: 15,
            }}>
            <View
              style={{
                width: 35,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={require('../../assets/images/care.png')}
                style={{width: 35, height: 35}}
              />
            </View>
            <View style={{position: 'relative', flex: 1}}>
              <TextInput
                placeholder="What do you want to eat?"
                style={{
                  borderWidth: 1,
                  borderColor: '#E8E8E8',
                  borderRadius: 25,
                  height: 40,
                  fontSize: 13,
                  paddingLeft: 45,
                  paddingRight: 20,
                  backgroundColor: 'white',
                  marginRight: 18,
                  marginLeft: 18,
                }}
              />
              <Image
                source={require('../../assets/images/search.png')}
                style={{position: 'absolute', top: 5, left: 28}}
              />
            </View>
            <View
              style={{
                width: 35,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={require('../../assets/images/heart2.png')}
                style={{width: 35, height: 35}}
              />
            </View>
          </View>
        </View>
        <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
          {/* SALDO */}
          <View style={{marginHorizontal: 17, marginTop: 10, marginBottom: 20}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: '#2C5FB8',
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
                padding: 14,
              }}>
              {/* <Image source={require("../../../assets/icon/gopay.png")} /> */}
              <Text style={{fontSize: 17, fontWeight: 'bold', color: 'white'}}>
                Rp 50.000
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                paddingTop: 20,
                paddingBottom: 14,
                backgroundColor: '#2F65BD',
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
              }}>
              <View style={{flex: 1, alignItems: 'center'}}>
                <Image
                  source={require('../../assets/images/isi.png')}
                  style={{width: 25, height: 25}}
                />
                <Text
                  style={{
                    fontSize: 13,
                    fontWeight: 'bold',
                    color: 'white',
                    marginTop: 15,
                  }}>
                  Isi
                </Text>
              </View>
              <View style={{flex: 1, alignItems: 'center'}}>
                <Image
                  source={require('../../assets/images/riwayat.png')}
                  style={{width: 25, height: 25}}
                />
                <Text
                  style={{
                    fontSize: 13,
                    fontWeight: 'bold',
                    color: 'white',
                    marginTop: 15,
                  }}>
                  Riwayat
                </Text>
              </View>
              <View style={{flex: 1, alignItems: 'center'}}>
                <Image
                  source={require('../../assets/images/lainnya.png')}
                  style={{width: 25, height: 25}}
                />
                <Text
                  style={{
                    fontSize: 13,
                    fontWeight: 'bold',
                    color: 'white',
                    marginTop: 15,
                  }}>
                  Lainnya
                </Text>
              </View>
            </View>
          </View>
          {/* <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <SliderBox
              images={images}
              parentWidth={350}
              sliderBoxHeight={150}
              autoplay={true}
            />
          </View> */}

          <SliderBox
            images={images}
            parentWidth={374}
            sliderBoxHeight={150}
            autoplay
            circleLoop
            autoplayInterval={10000}
            resizeMethod={'resize'}
            resizeMode={'cover'}
            paginationBoxStyle={{
              position: 'absolute',
              bottom: 0,
              padding: 0,
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              paddingVertical: 10,
            }}
            ImageComponentStyle={{
              // borderRadius: 15,
              width: '100%',
              marginTop: 5,
              marginLeft: 30,
            }}
          />
          {/* SCROLL MENU 1*/}
          <View>
            <View style={{height: 15, width: 60, marginLeft: 16}}></View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 16,
                paddingHorizontal: 16,
              }}>
              <Text
                style={{fontSize: 17, fontWeight: 'bold', color: '#1C1C1C'}}>
                Menu
              </Text>
            </View>
            <ScrollView
              horizontal={true}
              style={{
                flexDirection: 'row',
                paddingLeft: 16,
                paddingBottom: 20,
              }}>
              <View>
                <TouchableOpacity onPress={() => navigation.navigate('Donasi')}>
                  <View style={{width: 150, height: 70, borderRadius: 10}}>
                    <Image
                      source={require('../../assets/images/donasi.png')}
                      style={{
                        width: 70,
                        width: 70,
                        resizeMode: 'cover',
                        flex: 1,
                        borderRadius: 4,
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'bold',
                      color: '#1C1C1C',
                      marginTop: 12,
                    }}>
                    Donasi
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity onPress={() => navigation.navigate('Chart')}>
                  <View style={{width: 150, height: 70, borderRadius: 10}}>
                    <Image
                      source={require('../../assets/images/statistic.png')}
                      style={{
                        width: 70,
                        width: 70,
                        resizeMode: 'cover',
                        flex: 1,
                        borderRadius: 4,
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'bold',
                      color: '#1C1C1C',
                      marginTop: 12,
                    }}>
                    Statistik
                  </Text>
                </TouchableOpacity>
              </View>

              <View>
                <TouchableOpacity
                  onPress={() => navigation.navigate('History')}>
                  <View style={{width: 150, height: 70, borderRadius: 10}}>
                    <Image
                      source={require('../../assets/images/riwayat2.png')}
                      style={{
                        width: 70,
                        width: 70,
                        resizeMode: 'cover',
                        flex: 1,
                        borderRadius: 4,
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'bold',
                      color: '#1C1C1C',
                      marginTop: 12,
                    }}>
                    Riwayat
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity
                  onPress={() => navigation.navigate('CreateDonation')}>
                  <View style={{width: 150, height: 70, borderRadius: 10}}>
                    <Image
                      source={require('../../assets/images/bantu.png')}
                      style={{
                        width: 70,
                        width: 70,
                        resizeMode: 'cover',
                        flex: 1,
                        borderRadius: 4,
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'bold',
                      color: '#1C1C1C',
                      marginTop: 12,
                    }}>
                    Bantu
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <View style={{width: 150, height: 70, borderRadius: 10}}>
                  <Image
                    source={require('../../assets/images/category.png')}
                    style={{
                      width: 70,
                      width: 70,
                      resizeMode: 'cover',
                      flex: 1,
                      borderRadius: 4,
                    }}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: '#1C1C1C',
                    marginTop: 12,
                  }}>
                  Kategori
                </Text>
              </View>
              <View style={{width: 10}}></View>
            </ScrollView>
            <View
              style={{
                marginTop: 16,
                borderBottomColor: '#E8E9ED',
                borderBottomWidth: 1,
                marginHorizontal: 16,
                marginBottom: 20,
              }}></View>
          </View>
          {/* SCROLL MENU Image*/}
          <View>
            <View style={{height: 15, width: 60, marginLeft: 16}}></View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 16,
                paddingHorizontal: 16,
              }}>
              <Text
                style={{fontSize: 17, fontWeight: 'bold', color: '#1C1C1C'}}>
                Penggalangan Dana Mendesak
              </Text>
            </View>
            <ScrollView
              horizontal={true}
              style={{
                flexDirection: 'row',
                paddingLeft: 16,
                paddingBottom: 20,
              }}>
              <View>
                <View style={{width: 150, height: 70, borderRadius: 10}}>
                  <Image
                    source={require('../../assets/dummy/danamendesak1.jpg')}
                    style={{
                      width: 70,
                      height: 70,
                      resizeMode: 'cover',
                      flex: 1,
                      borderRadius: 4,
                    }}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: '#1C1C1C',
                    marginTop: 12,
                  }}>
                  Bantu Rifaldo
                </Text>
              </View>
              <View>
                <View style={{width: 150, height: 70, borderRadius: 10}}>
                  <Image
                    source={require('../../assets/dummy/danamendesak2.jpg')}
                    style={{
                      width: 90,
                      height: 70,
                      resizeMode: 'cover',
                      flex: 1,
                      borderRadius: 4,
                    }}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: '#1C1C1C',
                    marginTop: 12,
                  }}>
                  Banjir Rokan
                </Text>
              </View>
              <View>
                <View style={{width: 150, height: 70, borderRadius: 10}}>
                  <Image
                    source={require('../../assets/dummy/danamendesak3.jpg')}
                    style={{
                      width: 90,
                      height: 70,
                      resizeMode: 'cover',
                      flex: 1,
                      borderRadius: 4,
                    }}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: '#1C1C1C',
                    marginTop: 12,
                  }}>
                  Tsunami Palu
                </Text>
              </View>
              <View>
                <View style={{width: 150, height: 70, borderRadius: 10}}>
                  <Image
                    source={require('../../assets/dummy/danamendesak4.jpg')}
                    style={{
                      width: 90,
                      height: 70,
                      resizeMode: 'cover',
                      flex: 1,
                      borderRadius: 4,
                    }}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: '#1C1C1C',
                    marginTop: 12,
                  }}>
                  Covid 19 Nakes
                </Text>
              </View>

              <View style={{width: 10}}></View>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default Home;
