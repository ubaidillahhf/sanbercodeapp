import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3A86FF',
  },
  quotesContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  quotes: {
    fontSize: 14,
    color: 'white',
  },
  logo: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  textLogo: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default styles;
