const URI = 'https://crowdfunding.sanberdev.com';
const API = 'https://crowdfunding.sanberdev.com/api';
const API_GET_VENUE = 'https://crowdfunding.sanberdev.com/api';
const API_UPDATE_PROFILE = 'https://crowdfunding.sanberdev.com/api';

export {URI, API, API_GET_VENUE, API_UPDATE_PROFILE};
