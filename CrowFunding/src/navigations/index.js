import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Ionicons from 'react-native-vector-icons/AntDesign';

// Splash Screen
import SplashScreens from '../screens/SplashScreen/SplashScreen';
// Main
import Intro from '../screens/Intro/Intro';
import Login from '../screens/Login';
// Account
import Account from '../screens/Account';
import EditAccount from '../screens/Account/edit';
import RegisterAccount from '../screens/Account/registerAccount';
import InputOTP from '../screens/Account/inputOTP';
import SetupPassword from '../screens/Account/setupPassword';
import ResetPassword from '../screens/Account/resetPassword';
import Help from '../screens/Account/help';
import {colors} from '../style/colors';
// Home
import Home from '../screens/Home';
// Donasi
import Donasi from '../screens/Donasi';
import DetailDonasi from '../screens/Donasi/detailDonasi';
import Payment from '../screens/Donasi/payment';
// Maps
import Maps from '../screens/Maps';
// Chart
import ReactNative from '../screens/ReactNative';
// Inbox
import Inbox from '../screens/Inbox';
import AsyncStorage from '@react-native-async-storage/async-storage';
//Riwayat Donasi
import History from '../screens/Donasi/history';
//Buat Donasi
import CreateDonation from '../screens/Donasi/create';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const MainNavigation = (props) => (
  <Stack.Navigator initialRouteName={props.token !== null ? 'Home' : 'Intro'}>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{
        title: 'Masuk',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="RegisterAccount"
      component={RegisterAccount}
      options={{
        title: 'Daftar',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="InputOTP"
      component={InputOTP}
      options={{
        title: 'Masukkan OTP',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="SetupPassword"
      component={SetupPassword}
      options={{
        title: 'Buat Password',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="ResetPassword"
      component={ResetPassword}
      options={{
        title: 'Reset Password',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="Donasi"
      component={Donasi}
      options={{
        title: 'Donasi',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="DetailDonasi"
      component={DetailDonasi}
      options={{
        title: 'Detail Donasi',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="Payment"
      component={Payment}
      options={{
        title: 'Pembayaran',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="Chart"
      component={ReactNative}
      options={{
        title: 'Statistik',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />

    <Stack.Screen
      name="Home"
      options={{headerShown: false}}
      component={BottomTabNavigator}
    />
  </Stack.Navigator>
);

const HomeNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Home"
      component={Home}
      options={{headerShown: false}}
      // options={{
      //   title: 'Reset Password',
      //   headerStyle: {backgroundColor: colors.blue},
      //   headerTintColor: colors.white,
      // }}
    />
    <Stack.Screen
      name="CreateDonation"
      component={CreateDonation}
      options={{
        title: 'Buat Donasi',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="History"
      component={History}
      options={{
        title: 'Riwayat',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
  </Stack.Navigator>
);

const InboxNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Inbox"
      component={Inbox}
      // options={{headerShown: false}}
    />
  </Stack.Navigator>
);
// const MapsNavigation = () => (
//   <Stack.Navigator>
//     <Stack.Screen name="Maps" component={Maps} options={{headerShown: false}} />
//   </Stack.Navigator>
// );

const AccountNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Account"
      component={Account}
      options={{
        title: 'Account',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="EditAccount"
      component={EditAccount}
      options={{
        title: 'Edit Account',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
    <Stack.Screen
      name="Help"
      component={Help}
      options={{
        title: 'Bantuan',
        headerStyle: {backgroundColor: colors.blue},
        headerTintColor: colors.white,
      }}
    />
  </Stack.Navigator>
);

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home';
          } else if (route.name === 'Account') {
            iconName = focused ? 'user' : 'user';
          } else {
            iconName = focused ? 'inbox' : 'inbox';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}>
      <Tab.Screen name="Home" component={HomeNavigation} />
      <Tab.Screen name="Inbox" component={InboxNavigation} />
      <Tab.Screen name="Account" component={AccountNavigation} />
    </Tab.Navigator>
  );
};

const AppNavigation = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [token, setToken] = useState(null);
  const [user, setUser] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);

    async function getToken() {
      const token = await AsyncStorage.getItem('token');
      setToken(token);
    }

    getToken();
  }, []);

  if (isLoading) {
    return <SplashScreens />;
  }

  return (
    <NavigationContainer>
      <MainNavigation token={token} />
    </NavigationContainer>
  );
};

export default AppNavigation;
